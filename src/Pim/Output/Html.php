<?php

namespace Pim\Output;

use Doctrine\Common\EventSubscriber;
use Pim\Event\CursorMoved;

class Html implements EventSubscriber
{
    private $cursorWidth;
    private $cursorHeight;

    public function getSubscribedEvents()
    {
        return [
            'CursorMoved',
        ];
    }

    public function CursorMoved(CursorMoved $move)
    {
        $this->cursorWidth = $move->line();
        $this->cursorHeight = $move->column();
        $this->redraw();
    }

    private function redraw()
    {
        (var_dump($this->cursorHeight, $this->cursorWidth));
        file_put_contents('/tmp/pim/html', sprintf('
            <script>setTimeout(function() { window.location.reload(); }, 500);</script>
            <div style="color: black; top: %dem; left: %dem; position: absolute">X</div>
<pre>
        ', $this->cursorHeight, $this->cursorWidth)
        );
    }
}
