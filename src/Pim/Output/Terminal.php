<?php

namespace Pim\Output;

use Doctrine\Common\EventSubscriber;

class Terminal implements EventSubscriber
{
    private $width;
    private $height;

    public function __construct($width = 80, $height = 80)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getSubscribedEvents()
    {
        return [
            'ResourceOpened',
            'CursorMoved',
        ];
    }

    public function CursorMoved(Cursor $cursor)
    {
        $this->redraw();
    }

    private function redraw()
    {
        echo '';
    }
}
