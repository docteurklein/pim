<?php

namespace Pim;

use Pim\Resource\Resolver;
use Pim\Event;
use Pim\EventDispatcher;
use Pim\Command;
use Pim\Editor\Cursor;

class Editor
{
    const MODE_INSERT = 'insert';
    const MODE_NORMAL = 'normal';

    private $mode = self::MODE_NORMAL;
    private $resolver;
    private $dispatcher;
    private $cursor;

    public function __construct(Resolver $resolver, EventDispatcher $dispatcher)
    {
        $this->resolver = $resolver;
        $this->dispatcher = $dispatcher;
        $this->cursor = new Cursor(1, 1);
        $this->dispatcher->raise(new Event\CursorMoved($this->cursor));
    }

    public function edit($id)
    {
        $resource = $this->resolver->resolve($id);
        $this->resources[$id] = $resource;
        $this->dispatcher->raise(new Event\ResourceOpened($resource));
    }

    public function write($id)
    {
        $resource = $this->resolver->resolve($id);
        $this->resources[$id] = $resource;
        $this->dispatcher->raise(new Event\ResourceWritten($resource));
    }

    public function insert($character)
    {
        file_put_contents('/tmp/pim/html', $character, FILE_APPEND);
    }

    public function obey(Command $command)
    {
        // TODO make the command do it
        switch (get_class($command)) {
            case 'Pim\Command\MoveLeft':
                $this->cursor->left();
                return $this->dispatcher->raise(new Event\CursorMoved($this->cursor));
            case 'Pim\Command\MoveRight':
                $this->cursor->right();
                return $this->dispatcher->raise(new Event\CursorMoved($this->cursor));
            case 'Pim\Command\MoveUp':
                $this->cursor->up();
                return $this->dispatcher->raise(new Event\CursorMoved($this->cursor));
            case 'Pim\Command\MoveDown':
                $this->cursor->down();
                return $this->dispatcher->raise(new Event\CursorMoved($this->cursor));
            case 'Pim\Command\ModeInsert':
                return $this->switchMode(self::MODE_INSERT);
            case 'Pim\Command\ModeNormal':
                return $this->switchMode(self::MODE_NORMAL);
            default:
                throw new \Exception('Invalid command:'. get_class($command));
        }
    }

    public function switchMode($mode)
    {
        $this->mode = $mode;
    }

    public function isInserting()
    {
        return $this->mode === self::MODE_INSERT;
    }
}
