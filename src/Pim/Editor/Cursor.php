<?php

namespace Pim\Editor;

class Cursor
{
    private $line;
    private $column;

    public function __construct($line, $column)
    {
        $this->line = $line;
        $this->column = $column;
    }

    public function line()
    {
        return $this->line;
    }

    public function column()
    {
        return $this->column;
    }

    public function left()
    {
        $this->column = max(1, $this->column - 1);
    }

    public function right()
    {
        $this->column++;
    }

    public function up()
    {
        $this->line = max(1, $this->line - 1);
    }

    public function down()
    {
        $this->line++;
    }
}
