<?php

namespace Pim\Resource;

use Pim\Resource;

class Resolver
{
    public function resolve($id)
    {
        return new Resource(file_get_contents($id));
    }
}
