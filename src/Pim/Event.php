<?php

namespace Pim;

interface Event
{
    public function name();
}
