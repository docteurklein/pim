<?php

namespace Pim\Input;

use Pim\Command;
use Pim\Editor;

class StdIn
{
    private $buffer = '';
    private $commands = [];

    public function __construct(Editor $editor, array $commands = [])
    {
        $this->editor = $editor;
        $this->commands = $commands ?: [
            'h'    => new Command\MoveLeft,
            '[D' => new Command\MoveLeft,
            'j'    => new Command\MoveDown,
            '[B' => new Command\MoveDown,
            'k'    => new Command\MoveUp,
            '[A' => new Command\MoveUp,
            'l'    => new Command\MoveRight,
            '[C' => new Command\MoveRight,
            "i"    => new Command\ModeInsert,
            ''   => new Command\ModeNormal,
            'jj'   => new Command\ModeNormal,
        ];

        readline_callback_handler_install('', function() { });
        while (false !== $character = fgetc($fd = fopen('php://stdin', 'r'))) {
            var_dump(ord($character));
            $this->watch($character);
        }

        fclose($fd);
    }

    public function watch($character)
    {
        if ($this->editor->isInserting()) {
            return $this->editor->insert($character);
        }
        $this->buffer .= $character;
        if (in_array($this->buffer, array_keys($this->commands))) {
            $this->editor->obey($this->commands[$this->buffer]);
            $this->buffer = '';
        }
    }
}
