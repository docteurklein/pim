<?php

namespace Pim;

use Pim\Event;
use Pim\EventDispatcher;
use Doctrine\Common\EventManager;

class EventDispatcher
{
    private $manager;

    public function __construct(EventManager $manager)
    {
        $this->manager = $manager;
    }

    public function raise(Event $event)
    {
        $this->manager->dispatchEvent($event->name(), $event);
    }
}
