<?php

namespace Pim\Event;

use Pim\Event;

class ResourceOpened implements Event
{
    private $resource;

    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
    }

    public function name()
    {
        return (new \ReflectionClass(__CLASS__))->getShortClassName();
    }
}
