<?php

namespace Pim\Event;

use Pim\Editor\Cursor;
use Pim\Event;
use Doctrine\Common\EventArgs;

class CursorMoved extends EventArgs implements Event
{
    public function __construct(Cursor $cursor)
    {
        $this->cursor = $cursor;
    }

    public function line()
    {
        return $this->cursor->line();
    }

    public function column()
    {
        return $this->cursor->column();
    }

    public function name()
    {
        return (new \ReflectionClass(__CLASS__))->getShortName();
    }
}
